import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const [plans, setPlans] = useState("");

  useEffect(() => {  
    let plans =[
      {
        plan:"Bronze",
        amount: "35.99",
        items:[
          "Conference plans",
          "Easy Access",
          "Free Contacts"
        ]
      },
      {
        plan:"Silver",
        amount: "99.99",
        items:[
          "Conference plans",
          "Certificate",
          "Easy Access",
          "Free Contacts"
        ]
      },
      {
        plan:"Gold",
        amount: "199.99",
        items:[
          "Conference plans",
          "Free Lunch And Coffee",
          "Certificate",
          "Easy Access",
          "Free Contacts"
        ]
      }
    ]                    
   setPlans(plans)
}, [])


  return (
    <div>
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
    <section class="pricing-section">
        <div class="container">
            <div class="sec-title text-center">
                <span class="title">Get pricing plan</span>
                <h2>Subscribe a Plan</h2>
            </div>

            <div class="outer-box">
                <div class="row">
                 
                     {/*  Block  */}
                     {
                       plans != ""?
                     plans.map((anObjectMapped, index) => {
                          return (
                              
                               <div key={index} class="pricing-block col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                               <div class="inner-box">
                                   <div class="icon-box">
                                       <div class="icon-outer"><i class="fas fa-gem"></i></div>
                                   </div>
                                   <div class="price-box">
                                       <div class="title">{anObjectMapped.plan}</div>
                                        <h4 class="price">${anObjectMapped.amount}</h4>
                                   </div>
                                   <ul class="features">
                                     {
                                       anObjectMapped.items.map((item, index) => (
                                        <li key={index} class="true">{item}</li>
                                    ))}
                                     
                                   </ul>
                                   <div class="btn-box">
                                       <a href="" class="theme-btn">Subscribe</a>
                                   </div>
                               </div>
                           </div>
                          );
                      }):
                      null
                    }
                   

                 
                </div>
            </div>
        </div>
    </section>

    </div>
  );
}

export default App;
